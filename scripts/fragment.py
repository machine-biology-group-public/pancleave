import pandas as pd

class Fragmenter:

    '''
    Objects of this class provide sequence fragmentation functionality.
    This includes sliding-window fragmentation and subsequence-based
    fragmentation.
    '''
    
    def slide_window_single(self, sequence, window_size = 8):
        
        '''
        This method fragments a single sequence by sliding a window
        of inputted size over the sequence. If the desired window size 
        is n, all n-length contiguous subsequences will be returned.
        
        Parameters:
            sequence: a single interable sequence (e.g. a string, a 
                list of ints).
            window_size (default = 8): integer indicating desired 
                size of each subsequence. This value must be an even 
                number.
            
        Return:
            frag_list: list of subsequences each of size = window_size.
        '''
        
        try:
            # Edge cases.
            if (window_size > len(sequence)):
                return [None]
            if (window_size == len(sequence)):
                return [sequence] 
            
            # Store fragments in a list.
            frag_list = [None] * (len(sequence) - window_size + 1)
            for i in range(len(sequence) - window_size + 1):
                fragment = sequence[i:(i + window_size)]
                frag_list[i] = fragment
        except:
            return [None]
            
        return frag_list
    
    
    def slide_window(self, 
                     sequences, 
                     window_size = 8, 
                     return_type = "list"):
        
        '''
        This method fragments multiple sequences by sliding a window
        of inputted size over the sequence. If the desired window size 
        is n, all n-length contiguous subsequences will be returned.
        
        Helper functions: 
            slide_window_single(self, sequence, window_size = 8)
        
        Parameters:
            sequences: a list of interable sequences (len >= 1).
            window_size (default = 8): integer indicating desired 
                size of each subsequence. This value must be an even 
                number.
            return_type (default = "list"): string indicating which 
                data structure is desired on return. Valid values
                are "list", "dataframe", or "dictionary".
            
        Return:
            Data type as indicated by return_type. 
            
            If list is indicated, a list of lists containing subsequences 
            will be returned. The index of each subsequence sublist will 
            correspond to the index of the original sequence in the input 
            list.
            
            If dataframe is indicated, the return value will have a column 
            for the input sequences and a column for subsequences. 
            
            If dictionary is indicated, the return value will map sequences
            (keys) to a list of subsequences (values).
        '''
        
        # Construct list of lists containing subsequences.
        frag_list = [self.slide_window_single(seq, window_size) for seq in sequences]
        
        # Return as desired data type.
        if (return_type == "list"):
            return frag_list
        
        if (return_type == "dataframe"):
            return pd.DataFrame(zip(sequences, frag_list), 
                                columns = ["Sequence", "Subsequences"])
        
        if (return_type == "dictionary"):
            return dict(zip(sequences, frag_list))
        
        
    def cleave_single(self, 
                      sequence, 
                      cleavage_sites, 
                      site_len = 8):
        
        '''
        Computational protease! This method cleaves a given sequence
        at every cleavage site provided.
        
        Parameters:
            sequence: a single string.
            cleavage_sites: a list of cleavage sites each of length site_len.
            site_len (default = 8): integer indicating the size of each 
                cleavage site. A site of length n indicates that a cleavage 
                event takes place after the (n/2)th position. This value must
                be an even number.
                
        Return:
            fragments: a list of strings indicating the fragments resulting
                from proteolytic cleavage.
        '''
        
        # Test for valid cleavage site length.
        if (site_len % 2 != 0):
            print("Error: site_len must be an even integer.")
            return None
        
        # Insert space at every cleavage site to serve as delimiter.
        split_seq = "".join(sequence)
        split_count = 0
        for i in range(len(sequence) - site_len + 1):
            fragment = sequence[i:(i + site_len)]
            if (fragment in cleavage_sites):
                loc = i + int(site_len/2) + split_count
                split_seq = split_seq[:loc] + " " + split_seq[loc:]
                split_count += 1
        
        # Split sequence at every white space.
        fragments = split_seq.split()
        
        return fragments
    

    def cleave(self, 
               sequences, 
               cleavage_sites, 
               site_len = 8, 
               return_type = "list"):
        
        '''
        Computational protease! This method cleaves a list of sequences
        at every cleavage site provided per sequence.
        
        Parameters:
            sequences: a list of strings.
            cleavage_sites: a list of lists of cleavage sites per sequence
                in `sequences`. The index of each cleavage site sublist will 
                correspond to the index of the original sequence in `sequences`.
            site_len (default = 8): integer indicating the size of each 
                cleavage site. A site of length n indicates that a cleavage 
                event takes place after the (n/2)th position. This value must
                be an even number.
            return_type (default = "list"): string indicating which data 
                structure is desired on return. Valid values are "list", 
                "dataframe", or "dictionary".
            
        Return:
            Data type as indicated by return_type. 
            
            If list is indicated, a list of lists containing fragments 
            will be returned. The index of each cleaved fragment sublist will 
            correspond to the index of the original sequence in the input 
            list.
            
            If dataframe is indicated, the return value will have a column 
            for the input sequences and a column for cleaved fragments. 
            
            If dictionary is indicated, the return value will map sequences
            (keys) to a list of cleaved fragments (values).
        '''
        
        # Construct list of lists containing cleaved fragments.
        frag_list = [self.cleave_single(seq, sites, site_len) for seq, sites in zip(sequences, cleavage_sites)]
        
        # Return as desired data type.
        if (return_type == "list"):
            return frag_list
        
        if (return_type == "dataframe"):
            return pd.DataFrame(zip(sequences, frag_list), 
                                columns = ["Sequence", "Fragments"])
        
        if (return_type == "dictionary"):
            return dict(zip(sequences, frag_list))
        
        print("Invalid value passed as return_value. Valid values are 'list', 'dataframe', or 'dictionary'.")
        return None