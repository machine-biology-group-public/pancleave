import pandas as pd
import numpy as np
from encode import Encoder

class Classifier:

    '''
    Objects of this class define the trained classifier model and provide
    functionality for binary classification.
    '''

    def __init__(self, model = "panCleave"):
        
        '''
        Constructor.
        
        Parameters:
            model (default = "panCleave"): string indicating name of desired
                classifier. This model is already trained.
        '''
        
        if (model == "panCleave"):
            self.model = pd.read_pickle(r"panCleave_rf.pkl")
    
    
    def classify(self, 
                 sequences, 
                 raw = True, 
                 probability = True,
                 return_type = "array"):
        
        '''
        This method classifies inputted sequences as cleavage sites or
        non-cleavage sites. Initial sequences must be 8 residues long.
        If inputs are raw (i.e. amino acid sequence strings), they will
        be encoded to ProtFP float matrices prior to prediction.
        
        Parameters:
            sequences: a list of 8-residue peptides.
            raw (default = True): boolean indicating whether input is
                ProtFP encoded (False) or unencoded (True).
            probability (default = True): booliean indicating whether to
                to return class membership probability estimates (True)
                or not (False).
            return_type (default = "array"): string indicating which data 
                structure is desired on return. Valid values are "array", 
                "list", or "dataframe".
            
        Return:
            Data type as indicated by return_type. 
            
            If array is indicated, an array of predicted labels is returned. 
            The index of each label prediction will correspond to the index 
            of the original sequence in the input list. If probability == True,
            an array of probability estimates is also returned.
            
            If list is indicated, a list predicted labels will be returned. 
            The index of each label prediction will correspond to the index 
            of the original sequence in the input list. If probability == True,
            a list of probability estimates is also returned.
            
            If dataframe is indicated, the return value will have a column 
            for the input sequences and a column for predicted class labels. 
            If probability == True, a column of probability estimates is also
            included.
            
        Examples: 
            df = panCleave.classify(encoded_list, 
                                    raw = False, 
                                    return_type = "dataframe")
            predictions, probabilities = panCleave.classify(seq_list)
        '''
        
        # Encode if necessary, then predict.
        if (raw == True):
            protFP = Encoder(mapping = "protfp")
            # Remove invalid sequences (too short or noncanonical).
            sequences = [seq for seq in sequences]
            encodings = protFP.encode_list(sequences, flatten = True)
            predictions = self.model.predict(encodings)
            if (probability == True):
                probabilities = list(self.model.predict_proba(encodings))
                probabilities = [pr.max() for pr in probabilities]
        else:
            predictions = self.model.predict(sequences)
            probabilities = self.model.predict_proba(sequences)
            if (probability == True):
                probabilities = list(self.model.predict_proba(sequences))
                probabilities = [pr.max() for pr in probabilities]
        
        # Return desired data type.
        if (return_type == "array"):
            if (probability == True):
                return predictions, np.array(probabilities)
            else:
                return predictions
        if (return_type == "list"):
            if (probability == True):
                return list(predictions), probabilities
            else:
                return list(predictions)
        if (return_type == "dataframe"):
            if (raw == True):
                if (probability == True):
                    return pd.DataFrame(zip(sequences, encodings, 
                                            predictions, probabilities), 
                                        columns = ["Sequence", "Encoding", 
                                                   "Prediction", "Probability"])
                else:
                    return pd.DataFrame(zip(sequences, encodings, predictions), 
                                        columns = ["Sequence", "Encoding", "Prediction"])
            if (probability == True):
                return pd.DataFrame(zip(sequences, predictions, probabilities), 
                                    columns = ["Encoding", "Prediction", "Probability"])
            return pd.DataFrame(zip(sequences, predictions), 
                                columns = ["Encoding", "Prediction"])