class Encoder:

    '''
    Objects of this class provide amino acid encoding functionality.
    '''
    
    def __init__(self, mapping):

        '''
        Constructor.

        Parameters:
            mapping: the only instance variable for objects of this
                     class; a string indicating which encoding method
                     is desired. Valid inputs are "zscale", "protfp",
                     "stscale", and "onehot". This will dictate the
                     dictionary mapping amino acids (as chars) to 1D
                     feature vectors (as lists of doubles).
        '''

        if (mapping == "zscale"):
            self.mapping = self.get_zscale()
        elif (mapping == "stscale"):
            self.mapping = self.get_stscale()
        elif (mapping == "protfp"):
            self.mapping = self.get_protfp()
            

    def get_alphabet_single(self):

        '''
        Getter for 1-letter code amino acid alphabet.
        '''

        return ['A', 'C', 'D', 'E', 'F','G', 'H', 'I', 'K', 'L',
                'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']


    def get_alphabet_triple(self):

        '''
        Getter for 3-letter code amino acid alphabet.
        '''

        return ['Ala', 'Cys', 'Asp', 'Glu', 'Phe',
                'Gly', 'His', 'Ile', 'Lys', 'Leu',
                'Met', 'Asn', 'Pro', 'Gln', 'Arg',
                'Ser', 'Thr', 'Val', 'Trp', 'Tyr']


    def get_alphabet_full(self):

        '''
        Getter for full-name amino acid alphabet.
        '''

        return ['Alanine', 'Cysteine', 'Aspartic Acid', 'Glutamic Acid',
                'Phenylalanine', 'Glycine', 'Histidine', 'Isoleucine',
                'Lysine', 'Leucine', 'Methionine', 'Asparagine',
                'Proline', 'Glutamine', 'Arginine', 'Serine',
                'Threonine', 'Valine', 'Tryptophan', 'Tyrosine']


    def get_aa_conversion(self):

        '''
        Getter for dictionary mapping full-name amino acids to 3-letter code
        and 1-letter code.
        '''

        aa_dict = {'Alanine': ('Ala', 'A'),
                   'Cysteine': ('Cys', 'C'),
                   'Aspartic Acid': ('Asp', 'D'),
                   'Glutamic Acid': ('Glu', 'E'),
                   'Phenylalanine': ('Phe', 'F'),
                   'Glycine': ('Gly', 'G'),
                   'Histidine': ('His', 'H'),
                   'Isoleucine': ('Ile', 'I'),
                   'Lysine': ('Lys', 'K'),
                   'Leucine': ('Leu', 'L'),
                   'Methionine': ('Met', 'M'),
                   'Asparagine': ('Asn', 'N'),
                   'Proline': ('Pro', 'P'),
                   'Glutamine': ('Gln', 'Q'),
                   'Arginine': ('Arg', 'R'),
                   'Serine': ('Ser', 'S'),
                   'Threonine': ('Thr', 'T'),
                   'Valine': ('Val', 'V'),
                   'Tryptophan': ('Trp', 'W'),
                   'Tyrosine': ('Tyr', 'Y')}

        return aa_dict


    def get_zscale(self):

        '''
        This method is a getter for the dictionary mapping
        zscale three-dimensional feature vectors (as lists) to
        single-letter amino acid code.

        Parameters: none.

        Return: single_dict, a dictionary of string values mapped
                to double list keys.
        '''

        single_dict = {"A": [0.07, -1.73, 0.09],
                       "V": [-2.69, -2.53, -1.29],
                       "L": [-4.19, -1.03, -0.98],
                       "I": [-4.44, -1.68, -1.03],
                       "P": [-1.22, 0.88, 2.23],
                       "F": [-4.92, 1.3, 0.45],
                       "W": [-4.75, 3.65, 0.85],
                       "M": [-2.49, -0.27, -0.41],
                       "K": [2.84, 1.41, -3.14],
                       "R": [2.88, 2.52, -3.44],
                       "H": [2.41, 1.74, 1.11],
                       "G": [2.23, -5.36, 0.3],
                       "S": [1.96, -1.63, 0.57],
                       "T": [0.92, -2.09, -1.4],
                       "C": [0.71, -0.97, 4.13],
                       "Y": [-1.39, 2.32, 0.01],
                       "N": [3.22, 1.45, 0.84],
                       "Q": [2.18, 0.53, -1.14],
                       "D": [3.64, 1.13, 2.36],
                       "E": [3.08, 0.39, -0.07]}

        return single_dict


    def get_protfp(self):

        '''
        This method is a getter for the dictionary mapping
        fpscale eight-dimensional feature vectors (as lists) to
        single-letter amino acid code.

        Parameters: none.

        Return: single_dict, a dictionary of string values mapped
                to double list keys.
        '''

        single_dict = {"A": [-0.10, -4.94, -2.13, 1.70, -0.39, 1.06, -1.39, 0.97],
                       "C": [4.62, -3.54, 1.50, -1.26, 3.27, -0.34, -0.47, -0.23],
                       "D": [-6.61, 0.94, -3.04, -4.58, 0.48, -1.31, 0.10, 0.94],
                       "E": [-5.10, 2.20, -3.59, -2.26, -2.14, 1.35, -0.45, -1.31],
                       "F": [6.76, 0.88, 0.89, -1.12, -0.49, -0.55, -0.87, 1.05],
                       "G": [-5.70, -8.72, 4.18, -1.35, -0.31, 2.91, 0.32, -0.11],
                       "H": [0.17, 2.14, 1.20, 0.71, 1.16, -0.38, -1.85, -2.79],
                       "I": [6.58, -1.73, -2.49, 1.09, -0.34, -0.28, 1.97, -0.92],
                       "K": [-4.99, 5.00, 0.70, 3.00, -1.23, 1.41, 0.19, 0.87],
                       "L": [5.76, -1.33, -1.71, 0.63, -1.70, 0.71, -0.05, -0.51],
                       "M": [5.11, 0.19, -1.02, 0.15, 0.13, -0.30, -2.95, 0.50],
                       "N": [-4.88, 0.81, 0.14, -0.14, 1.23, -0.65, 1.02, -1.94],
                       "P": [-3.82, -2.31, 3.45, 1.00, -3.22, -3.54, -0.36, -0.30],
                       "Q": [-3.95, 2.88, -0.83, 0.52, 0.90, 0.55, -0.08, 0.64],
                       "R": [-2.79, 6.60, 1.21, 2.07, 1.67, 0.76, 0.00, 0.32],
                       "S": [-4.57, -2.55, -0.67, 1.11, 0.99, -1.02, 0.11, 0.65],
                       "T": [-2.00, -1.77, -0.70, 1.02, 1.06, -1.20, 0.74, 1.65],
                       "V": [5.04, -2.90, -2.29, 1.38, 0.06, 0.08, 1.79, -0.38],
                       "W": [7.33, 4.55, 2.77, -2.41, -1.08, 1.04, 0.23, 0.59],
                       "Y": [3.14, 3.59, 2.45, -1.27, -0.06, -0.29, 1.99, 0.30]}

        return single_dict


    def get_stscale(self):

        '''
        This method is a getter for the dictionary mapping
        stscale eight-dimensional feature vectors (as lists) to
        single-letter amino acid code.

        Parameters: none.

        Return: single_dict, a dictionary of string values mapped
                to double list keys.
        '''

        single_dict = {"A": [-1.552,-0.791,-0.627,0.237,-0.461,-2.229,0.283,1.221],
                       "R": [-0.059,0.731,-0.013,-0.096,-0.253,0.3,1.256,0.854],
                       "N": [-0.888,-0.057,-0.651,-0.214,0.917,0.164,-0.14,-0.166],
                       "D": [-0.907,-0.054,-0.781,-0.248,1.12,0.101,-0.245,-0.075],
                       "C": [-1.276,-0.401,0.134,0.859,-0.196,-0.72,0.639,-0.857],
                       "Q": [-0.622,0.228,-0.193,-0.105,0.418,0.474,0.172,0.408],
                       "E": [-0.629,0.39,-0.38,-0.366,0.635,0.514,0.175,0.367],
                       "G": [-1.844,-0.018,-0.184,0.573,-0.728,-3.317,0.166,2.522],
                       "H": [-0.225,0.361,0.079,-1.037,0.568,0.273,1.208,-0.001],
                       "I": [-0.785,-1.01,-0.349,-0.097,-0.402,1.091,-0.139,-0.764],
                       "L": [-0.826,-0.379,0.038,-0.059,-0.625,1.025,-0.229,-0.129],
                       "K": [-0.504,0.245,0.297,-0.065,-0.387,1.011,0.525,0.553],
                       "M": [-0.693,0.498,0.658,0.457,-0.231,1.064,0.248,-0.778],
                       "F": [-0.019,0.024,1.08,-0.22,-0.937,0.57,-0.357,0.278],
                       "P": [-1.049,-0.407,-0.067,-0.066,-0.813,-0.89,0.021,-0.894],
                       "S": [-1.343,-0.311,-0.917,-0.049,0.549,-1.533,0.166,0.28],
                       "T": [-1.061,-0.928,-0.911,-0.063,0.538,-0.775,-0.147,-0.717],
                       "W": [0.853,0.039,0.26,-1.163,0.16,-0.202,1.01,0.195],
                       "Y": [0.308,0.569,1.1,-0.464,-0.144,-0.354,-1.099,0.162],
                       "V": [-1.133,-0.893,-0.325,0.303,-0.561,-0.175,-0.02,-0.311]}

        return single_dict
    
    
    def test_valid(self, aa_sequence, desired_len = None, threshold = "equal"):
        
        '''
        This method tests whether an amino acid sequence is invalid,
        i.e. whether it contains noncanonical residues or is of improper
        length.
        
        Parameters:
            aa_sequence: string of one-letter code amino acids.
            desired_len (default = None): integer indicating required
                sequence length. If None (default), length will not be
                tested.
            threshold (default = "equal"): string indicating how to test
                for desired length. String "equal" indicates strict equality,
                "min" indicates greater than or equal to, and "max" indicates
                less than or equal to.
            
        Return: boolean indicating validity (True) or invalidity (False).
        '''
        
        # Edge cases.
        if (isinstance(aa_sequence, str) == False or aa_sequence == ""):
            return False
        
        # Test for lowercase letters, which can indicate noncanonical residue.
        if (aa_sequence.isupper() == False):
            return False
        
        # Test length, if specified.
        if (threshold == "equal"):
            if ((desired_len != None) and (len(aa_sequence) != desired_len)):
                return False
        if (threshold == "min"):
            if ((desired_len != None) and (len(aa_sequence) < desired_len)):
                return False
        if (threshold == "max"):
            if ((desired_len != None) and (len(aa_sequence) > desired_len)):
                return False
        
        # Else, test for noncanonical residues.
        valid_aa = set(self.get_alphabet_single())
        return valid_aa.issuperset(aa_sequence)

            
    def singlefy_seq(self, aa_sequence, input_code = "three"):

            '''
            This method converts a three-letter code or full-name amino acid
            sequence to its single-letter code equivalent.

            Parameters:
                aa_sequence: sequence as string. Do not input a list.
                input_code (default = "three"): string indicating whether input
                        is three-letter code ("three") or full-name ("full").

            Return:
                single_seq: single-letter code equivalent as string.
            '''

            # Convert to single-letter code.
            single_seq = ""
            alphabet_single = self.get_alphabet_single()

            if (input_code == "three"):
                # Remove all white spaces as precautionary.
                aa_sequence = aa_sequence.replace(" ", "")
                alphabet_triple = self.get_alphabet_triple()
                for i in range(0, len(aa_sequence) - 1, 3):
                    # Check for invalid (non-canonical) amino acids.
                    aa = aa_sequence[i:i + 3].capitalize()
                    if (aa not in alphabet_triple):
                        return "Error: invalid amino acid."
                    # Convert.
                    index = alphabet_triple.index(aa)
                    single_seq = single_seq + alphabet_single[index]

            if (input_code == "full"):
                tokens = aa_sequence.split(" ")
                print(tokens)
                alphabet_full = get_alphabet_full()
                for i in range(len(tokens)):
                    aa = tokens[i].capitalize()
                    print(aa)
                    # Check for invalid (non-canonical) amino acids.
                    if (aa not in alphabet_full):
                        return "Error: invalid amino acid."
                    # Convert.
                    index = alphabet_full.index(aa)
                    single_seq = single_seq + alphabet_single[index]

            # Return converted sequence.
            return single_seq
        
        
    def singlefy_list(self, seq_list, input_code = "three"):
            
            '''
            This method converts a list of three-letter code or full-name 
            amino acid sequences to a list of single-letter code equivalents.

            Parameters:
                seq_list: list of sequences as strings.
                input_code (default = "three"): string indicating whether input
                        is three-letter code ("three") or full-name ("full").

            Return:
                list of single-letter code strings.
            '''

            return [self.singlefy_seq(item, input_code = input_code) for item in seq_list]

        
    def singlefy_dataframe(self, 
                           df, 
                           new_col = "Sequence (1-letter code)", 
                           raw_col = "Sequence",
                           input_code = "three"):
        
        '''
        This method converts amino acid sequences from full-name or 
        three-letter code to single-letter code for an entire dataframe column.

        Parameters:
            df: pandas dataframe.
            new_col (default = "Sequence (1-letter code)"): string  indicating 
                name of constructed single-letter code column.
            raw_col (default = "Sequence"): string indicating name of column
                containing original sequences.
            input_code (default = "three"): string indicating whether input
                is three-letter code ("three") or full-name ("full").

        Return:
            df: updated dataframe.
        '''
        
        # Encode column.
        df[new_col] = df[raw_col].apply(lambda x: self.singlefy_seq(x, input_code = input_code))
        
        return df
    
    
    def pad_seq_column(self, df, seq_column):

        '''
        This method pads sequences in a dataframe column with zeros
        to meet a predefined length.

        Parameters:
            df: pandas dataframe.
            seq_column: string indicating name of column containing
                sequence data, each entry of list data type.

        Return: updated dataframe.
        '''

        # Compute maximum length sequence in column.
        N = df[seq_column].map(len).max()

        # Pad with zeros for equal lengths.
        error = "Error: invalid amino acid."
        pad_lambda = lambda vector: vector + ([0] * (N - len(vector))) if (vector != error) else None
        df[seq_column] = df[seq_column].apply(pad_lambda)
        
        return df
    
        
    def return_binary_vector(self, amino_acid):

        '''
        This method returns a binary integer vector
        representing the one-hot encoding of a single
        amino acid, inputted in 1-letter code, 3-letter
        code, or full form.

        Parameters:
            amino_acid: amino acid as string, first letter
                        only capitalized. Do not input list.
            input_code (default = "single"): string
                        specifying which alphabet is
                        inputted: 1-letter code ("single"),
                        3-letter code ("triple"), or the
                        full amino acid name ("full").

        Return:
            1 x 20 binary integer vector one-hot encoding.
        '''

        # Init base vector of zeros.
        base_vector = [0] * 20

        # Encode.
        aa_single = self.get_alphabet_single()
        if (amino_acid not in aa_single):
            return "Error: invalid amino acid."
        base_vector[aa_single.index(amino_acid)] = 1

        return base_vector


    def encode_sequence(self, aa_sequence, flatten = False):

        '''
        This method returns a list of doubles representing the
        numerical encoding of an amino acid sequence, inputted
        in 1-letter code. This method uses the amino acid mapping
        that is passed to the Encoder object at instantiation.

        Parameters:
            aa_sequence: amino acid sequence as string. Do not
                         input a list of sequences.
            flatten (default = False): boolean indicating whether to
                         flatten 2D encoding to 1D list.

        Return:
            encoding: n x m list of doubles, where n is total
                      residues and m is the length of an individual
                      residue's feature vector. If flatten = True,
                      this list is 1 x (n x m).
        '''

        # Return if sequence == None.
        if (aa_sequence == None):
            return "Error: invalid amino acid."
        
        # Precautionary: set to uppercase.
        aa_sequence = aa_sequence.upper()

        # Build encoded sequence.
        seq_encoding = list()
        valid_aa_list = self.get_alphabet_single()
        for i in range(len(aa_sequence)):
            # Check for invalid (non-canonical) amino acids.
            if (aa_sequence[i] not in valid_aa_list):
                return "Error: invalid amino acid."
            # Encode and append to sequence encoding.
            if (self.mapping == "onehot"):
                aa_encoding = return_binary_vector(aa_sequence[i])
            else:
                aa_encoding = self.mapping.get(aa_sequence[i])
            seq_encoding.append(aa_encoding)

        # Flatten 2D list if specified.
        if (flatten == True):
            return [item for sublist in seq_encoding for item in sublist]

        # Return encoding for single amino acid sequence.
        return seq_encoding


    def encode_list(self, 
                    seq_list, 
                    flatten = False, 
                    return_value = "list"):

        '''
        This method returns a dictionary of sequence:encoding
        key-value pairs. This method uses the amino acid mapping
        that is passed to the Encoder object at instantiation.

        Helper function:
            encode_sequence(self, aa_sequence, flatten = False)

        Parameters:
            seq_list: list of amino acid sequences as strings.
            flatten (default = False): boolean indicating whether to
                flatten 2D encoding to 1D list.
            return_type (default = "array"): string indicating which 
                data structure is desired on return. Valid values are 
                "list" or "dictionary".

        Return:
            Data type as indicated by return_type:
            encoded_list: list of encodings, where the index of each 
                          encoding will correspond to the index of the 
                          original sequence in the input list.
            encoded_dict: dictionary of sequence:encoding key-value
                          pairs, where each key is a string and each
                          value is an n x m list of doubles, where n
                          is total residues and m is the length of an
                          individual residue's feature vector. If
                          flatten = True, each value is 1 x (n x m).
        '''

        # Construct list of encodings.
        if (return_value == "list"):
            encoded_list = [None] * len(seq_list)
            for i in range(len(seq_list)):
                encoded_list[i] = self.encode_sequence(seq_list[i], flatten = flatten)
            return encoded_list
        
        # OR: Construct dictionary of sequence:encoding key-value pairs.
        if (return_value == "dictionary"):
            encoded_dict = {}
            for seq in seq_list:
                encoded_dict[seq] = self.encode_sequence(seq, flatten = flatten)
            return encoded_dict


    def encode_dataframe(self,
                         df,
                         encode_column = "Encoding",
                         raw_column = "Sequence",
                         flatten = False,
                         pad = False):

        '''
        This method encodes a column of a dataframe.

        Parameters:
            df: pandas dataframe.
            encode_column (default = "Encoding"): string indicating name of
                constructed encoding column.
            raw_column (default = "Sequence"): string indicating name of column
                containing raw, unencoded sequences.
            flatten (default = False): boolean indicating whether to
                                       flatten 2D encoding to 1D list.
            pad (default = False): boolean indicating whether to pad encoding
                                   with zeros to length of maximum length encoding.

        Return:
            df: updated dataframe.
        '''

        # Encode column.
        df[encode_column] = df[raw_column].apply(lambda x: self.encode_sequence(x, flatten = flatten))

        # Pad if desired.
        if (pad == True):
            df = self.pad_seq_column(df, encode_column)

        # Return updated dataframe.
        return df