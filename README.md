# **panCleave: A pan-protease cleavage site classifier for computational proteolysis**

Code base developed by Jacqueline Maasch for the following publication.

Jacqueline R.M.A. Maasch, Marcelo D.T. Torres, Marcelo C.R. Melo, Cesar de la Fuente-Nunez. [Molecular de-extinction of ancient antimicrobial peptides enabled by machine learning](https://www.sciencedirect.com/science/article/pii/S1931312823002962). *Cell Host & Microbe*, 2023. ISSN 1931-3128. [https://doi.org/10.1016/j.chom.2023.07.001](https://doi.org/10.1016/j.chom.2023.07.001)

>**Abstract:** Molecular de-extinction could offer avenues for drug discovery by reintroducing bioactive molecules that are no longer encoded by extant organisms. To prospect for antimicrobial peptides encrypted within extinct and extant human proteins, we introduce the panCleave random forest model for proteome-wide cleavage site prediction. Our model outperformed multiple protease-specific cleavage site classifiers for three modern human caspases, despite its pan-protease design. Antimicrobial activity was observed in vitro for modern and archaic protein fragments identified with panCleave. Lead peptides showed resistance to proteolysis and exhibited variable membrane permeabilization. Additionally, representative modern and archaic protein fragments showed anti-infective efficacy against A. baumannii in both a skin abscess infection model and a preclinical murine thigh infection model. These results suggest that machine-learning-based encrypted peptide prospection can identify stable, nontoxic peptide antibiotics. Moreover, we establish molecular de-extinction through paleoproteome mining as a framework for antibacterial drug discovery.

>**Keywords:** antimicrobial peptides; antibiotics; machine learning; protein engineering; drug discovery; hominins; Neanderthal; Denisovan; mouse models; antibiotic resistance


## Table of contents

* **[License](#License)** 
* **[About](#About)**  &nbsp; | &nbsp; [Overview](#Overview) | &nbsp; [Random forest classifier](#Random-forest-classifier) | &nbsp; [Data representation](#Data-representation)
* **[Data](#Data)**  &nbsp; | &nbsp; [Training and test sets](#Training-and-test-sets) | &nbsp; [Data representation](#Data-representation)
* **[Usage](#Usage)** &nbsp; | &nbsp; [Use cases](#Use-cases) &nbsp; | &nbsp; [Installation](#Installation)

## License

This free software is copyleft and licensed under the GNU General Public License v3.0 (<a href="https://choosealicense.com/licenses/gpl-3.0/" target="_blank">GPL-3</a>). Please consult the `LICENSE` file before using this software.

&#8593; [return to top](#panCleave)

## About

### Overview

The `panCleave` Python pipeline is a protein informatics tool that uses machine learning to perform **computational proteolysis**: the *in silico* fragmentation of human protein sequences into peptides.

This package implements a `scikit-learn`-based random forest classifier to predict the location of proteoylytic cleavage sites in amino acid sequences. The `panCleave` model is **trained and tested on all human protease substrates** in the [MEROPS Peptidase Database](https://www.ebi.ac.uk/merops/index.shtml) as of June 2020. This pan-protease approach is designed to facilitate protease-agnostic cleavage site recognition and proteome-scale searches. When presented with an 8-residue input, `panCleave` returns a **binary classification** indicating that the sequence is predicted to be a cleavage site or non-cleavage site. Additionally, `panCleave` returns the **estimated probability** of class membership. Through probability reporting, this classifier allows the user to filter by probability threshold, e.g. to bias toward predictions of high probability.

The `panCleave` pipeline performs the following procedure per input protein:

1. **Sliding window:** Every 8-residue contiguous subsequence of the protein sequence string is computed.
2. **Encoding:** Each subsequence is converted to a numerical feature vector using the ProtFP encoding method.
3. **Prediction:** The label and estimated probability of class membership is computed per subsequence.
4. **Fragmentation:** The full protein string is tokenized at each predicted cleavage site, yielding a list of peptide fragments.

Additional utility functions are also provided, including prediction filtering functionality and FASTA file conversion.

**View the domain model for this pipeline [here](https://gitlab.com/machine-biology-group/pancleave/-/blob/master/panCleave_domain_model.pdf).**

&#8593; [return to top](#panCleave)

### Random forest classifier

The panCleave random forest classifier underpins the computational proteolysis pipeline. All training and testing data can be found in `data/training_testing_data/`. Software versioning requirements for reproducibility are provided in `requirements.txt`. The model is implemented in `scikit-learn` and takes the following form:

```python
RandomForestClassifier(bootstrap = True,
                       ccp_alpha = 0.0,
                       class_weight = None,
                       criterion = "entropy",
                       max_depth = None,
                       max_features = 3,
                       max_leaf_nodes = None,
                       max_samples = None,
                       min_impurity_decrease = 0.0,
                       min_impurity_split = None,
                       min_samples_leaf = 5,
                       min_samples_split = 2,
                       min_weight_fraction_leaf = 0.0,
                       n_estimators = 400,
                       n_jobs = -1,
                       oob_score = False,
                       random_state = 5,
                       verbose = 0,
                       warm_start = False)
```

The preserved, trained model can be found in `model/panCleave_rf.pkl`. Performance metrics are as follows:

```python
Accuracy at probability >= 0.5: 0.7330512743024076

Accuracy at probability >= 0.6: 0.8191301556732467

Accuracy at probability >= 0.7: 0.8854132367467896

Accuracy at probability >= 0.8: 0.9400187441424555

Accuracy at probability >= 0.9: 0.9658536585365853
```

```python
Negative predictive value                 = 0.731624273983577

Precision / positive predictive value     = 0.7344953384677746

Sensitivity / recall / true positive rate = 0.7300564061240935

Specificity / true negative rate          = 0.7360467459198066
```

&#8593; [return to top](#panCleave)

## Data

### Training and test sets

Training and 10-fold cross-validation were performed using 80% of total observations (n = 39,707). The remaining 20% of observations were reserved as an independent test set (n = 9,927). The train-test split was stratified by label to ensure that each split maintained a label distribution representative of the entire dataset (50% positive observations, 50% negative observations). 

Training data are contained in the following files.
1. `pancleave/data/training_testing_data/pancleave_train_data.csv`
	- This file contains 8-residue long string sequences and their associated labels.
2. `pancleave/data/training_testing_data/panCleave_train_data_encoded.csv`
	- This file contains 8-residue long string sequences, their associated labels, and encodings.

Test data are contained in the following files.
1. `pancleave/data/training_testing_data/pancleave_test_data.csv`
	- This file contains 8-residue long string sequences and their associated labels.
2. `pancleave/data/training_testing_data/panCleave_test_data_encoded.csv`
	- This file contains 8-residue long string sequences, their associated labels, and encodings.
 
### Model accuracy

Model accuracy by protease, protease clan, protease family, and protease type are available in the directory `data/disaggregated_model_accuracy`.


&#8593; [return to top](#panCleave)

### Data representation
8-residue sequences must be inputted to panCleave as single-letter amino acid code strings, e.g. "RGRRAEPQ" or "AEFTTNLT". panCleave includes a utility function that will convert three-letter code amino acid strings to single-letter code if needed.

Internally, panCleave will then encode single-letter code strings using the [ProtFP encoding method](https://jcheminf.biomedcentral.com/articles/10.1186/1758-2946-5-41), as shown below:

```python
Ala (A): -0.10,-4.94,-2.13,1.70,-0.39,1.06,-1.39,0.97
Cys (C): 4.62,-3.54,1.50,-1.26,3.27,-0.34,-0.47,-0.23
Asp (D): -6.61,0.94,-3.04,-4.58,0.48,-1.31,0.10,0.94
Glu (E): -5.10,2.20,-3.59,-2.26,-2.14,1.35,-0.45,-1.31
Phe (F): 6.76,0.88,0.89,-1.12,-0.49,-0.55,-0.87,1.05
Gly (G): -5.70,-8.72,4.18,-1.35,-0.31,2.91,0.32,-0.11
His (H): 0.17,2.14,1.20,0.71,1.16,-0.38,-1.85,-2.79
Ile (I): 6.58,-1.73,-2.49,1.09,-0.34,-0.28,1.97,-0.92
Lys (K): -4.99,5.00,0.70,3.00,-1.23,1.41,0.19,0.87
Leu (L): 5.76,-1.33,-1.71,0.63,-1.70,0.71,-0.05,-0.51
Met (M): 5.11,0.19,-1.02,0.15,0.13,-0.30,-2.95,0.50
Asn (N): -4.88,0.81,0.14,-0.14,1.23,-0.65,1.02,-1.94
Pro (P): -3.82,-2.31,3.45,1.00,-3.22,-3.54,-0.36,-0.30
Gln (Q): -3.95,2.88,-0.83,0.52,0.90,0.55,-0.08,0.64
Arg (R): -2.79,6.60,1.21,2.07,1.67,0.76,0.00,0.32
Ser (S): -4.57,-2.55,-0.67,1.11,0.99,-1.02,0.11,0.65
Thr (T): -2.00,-1.77,-0.70,1.02,1.06,-1.20,0.74,1.65
Val (V): 5.04,-2.90,-2.29,1.38,0.06,0.08,1.79,-0.38
Trp (W): 7.33,4.55,2.77,-2.41,-1.08,1.04,0.23,0.59
Tyr (Y): 3.14,3.59,2.45,-1.27,-0.06,-0.29,1.99,0.30
```

This encoding was selected for conferring the highest accuracy among representations tested. The ProtFP encoded amino acid strings will then be predicted on by the panCleave random forest classifier.

&#8593; [return to top](#panCleave)

## Usage

### Use cases

This pipeline was experimentally validated for encrypted antimicrobial peptide prospection (Maasch et al, in preparation). It can be used to explore potential cleavage products of any human protein. Protease-specific accuracies are reported in `data/disaggregated_model_accuracy/` to guide protease-specific applications of this model.

### Tutorials

Consult the Jupyter notebook `tutorials/panCleave_tutorial_02-17-21.ipynb` for an interactive example of how to use the full pipeline.

&#8593; [return to top](#panCleave)
