import unittest
from encode import Encoder

class TestEncoder(unittest.TestCase):

    
    def test_singlefy_seq(self):
        # Instantiate encoder object.
        encoder = Encoder("zscale")
        
        # List of sequences to encode.
        triple_list = ["Ala Cys Asp", 
                       "Glu Phe Gly His", 
                       "Ile Lys", 
                       "Leu Met Asn Pro Gln Arg",
                       "Ser Thr Val", 
                       "Trp Txr"]
        
        # Test single sequence conversion to 1-letter code.
        single = encoder.singlefy_seq(triple_list[0], input_code = "three")
        self.assertEqual(single, "ACD")
        
        
    def test_singlefy_list(self):
        # Instantiate encoder object.
        encoder = Encoder("zscale")
        
        # List of sequences to encode.
        triple_list = ["Ala Cys Asp", 
                       "Glu Phe Gly His", 
                       "Ile Lys", 
                       "Leu Met Asn Pro Gln Arg",
                       "Ser Thr Val", 
                       "Trp Txr"]
        
        # Test list conversion to 1-letter code.
        list_test = encoder.singlefy_list(triple_list)
        correct = ["ACD", "EFGH", "IK", "LMNPQR", "STV", "Error: invalid amino acid."]
        self.assertEqual(list_test, correct)
        
        
    def test_test_valid(self):
        # Instantiate encoder object.
        encoder = Encoder("zscale")
        
        # Test sequence validity.
        seq0 = "YACT"
        seq1 = "GKKLRIX"
        test0 = encoder.test_valid(seq0)
        test1 = encoder.test_valid(seq0, desired_len = 5, threshold = "equal")
        test2 = encoder.test_valid(seq0, desired_len = 5, threshold = "min")
        test3 = encoder.test_valid(seq0, desired_len = 5, threshold = "max")
        test4 = encoder.test_valid(seq1)
        test5 = encoder.test_valid(seq1, desired_len = 3, threshold = "equal")
        test6 = encoder.test_valid(seq1, desired_len = 3, threshold = "min")
        test7 = encoder.test_valid(seq1, desired_len = 3, threshold = "max")
        self.assertEqual(test0, True)
        self.assertEqual(test1, False)
        self.assertEqual(test2, False)
        self.assertEqual(test3, True)
        self.assertEqual(test4, False)
        self.assertEqual(test5, False)
        self.assertEqual(test6, False)
        self.assertEqual(test7, False)
        
        
    def test_encode_list(self):
        # Instantiate encoder object.
        encoder = Encoder("zscale")
        
        # List of sequences to encode.
        seq_list = ["AKRH",
                    "YKLLKLLLPKLKGLLFKL", 
                    "PFWRRRIRIRR", 
                    "GEKLKKIGKKIKNFFQKL", 
                    "RCYCRRRFCVCR", 
                    "KWKSFLKTFKSAKKTVLHTAAKAISS", 
                    "HGLN", 
                    "CAKGVLA"]
        
        # Test Z-Scale encoding of sequence list.
        test_flat = encoder.encode_list([seq_list[0], seq_list[6]], 
                                        return_value = "list",
                                        flatten = True)
        test_2D = encoder.encode_list([seq_list[0], seq_list[6]], 
                                      return_value = "list",
                                      flatten = False)
        correctAKRH_flat = [0.07, -1.73, 0.09, 
                            2.84, 1.41, -3.14, 
                            2.88, 2.52, -3.44, 
                            2.41, 1.74, 1.11]
        correctHGLN_flat = [2.41, 1.74, 1.11, 
                            2.23, -5.36, 0.3, 
                            -4.19, -1.03, -0.98, 
                            3.22, 1.45, 0.84]
        correctAKRH_2D = [[0.07, -1.73, 0.09], 
                          [2.84, 1.41, -3.14], 
                          [2.88, 2.52, -3.44], 
                          [2.41, 1.74, 1.11]]
        correctHGLN_2D = [[2.41, 1.74, 1.11], 
                          [2.23, -5.36, 0.3], 
                          [-4.19, -1.03, -0.98], 
                          [3.22, 1.45, 0.84]]
        correct_flat = [correctAKRH_flat, correctHGLN_flat]
        correct_2D = [correctAKRH_2D, correctHGLN_2D]
        
        self.assertEqual(test_flat, correct_flat)
        self.assertEqual(test_2D, correct_2D)
     
    
if (__name__ == "__main__"):
    unittest.main()