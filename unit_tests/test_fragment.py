import unittest
from fragment import Fragmenter
import pandas


class TestFragmenter(unittest.TestCase):
    
    
    def test_slide_window_single(self):
        # Instantiate Fragmenter object.
        frag = Fragmenter()
        
        # Test single sequence fragmentation.
        test_single0 = frag.slide_window_single([8937, 98329, 38, 28902], 3)
        test_single1 = frag.slide_window_single(8937, 3)
        test_single2 = frag.slide_window_single("KWKSFL", 2)
        
        self.assertEqual(test_single0, [[8937, 98329, 38], [98329, 38, 28902]])
        self.assertEqual(test_single1, [None])
        self.assertEqual(test_single2, ["KW", "WK", "KS", "SF", "FL"])
        
        
    def test_slide_window(self):
        # Instantiate Fragmenter object.
        frag = Fragmenter()
        
        # List of test sequences to fragment.
        seq_list = ["AKRHLKL",
                    "YKLL", 
                    "PFWR", 
                    "GEK",
                    "",
                    "X"]
        
        # Test fragmentation of multiple sequences: return list.
        subseq_list = frag.slide_window(seq_list, window_size = 3)
        correct_list = [["AKR", "KRH", "RHL", "HLK", "LKL"],
                        ["YKL", "KLL"],
                        ["PFW", "FWR"],
                        ["GEK"], 
                        [None],
                        [None]]
        self.assertEqual(subseq_list, correct_list)
        self.assertEqual(isinstance(subseq_list, list), True)
        
        # Test fragmentation of multiple sequences: return dataframe (success).
        subseq_df = frag.slide_window(seq_list, window_size = 3, return_type = "dataframe")
        self.assertEqual(isinstance(subseq_df, pandas.DataFrame), True)
        
        # Test fragmentation of multiple sequences: return dictionary (success).
        subseq_dict = frag.slide_window(seq_list, window_size = 3, return_type = "dictionary")
        self.assertEqual(isinstance(subseq_dict, dict), True)
        
        
    def test_cleave_single(self):
        # Instantiate Fragmenter object.
        frag = Fragmenter()
        
        # Test cleavage on a single sequence.
        cleave_test = frag.cleave_single("HGLNFLKTFKSRF", ["HGLNFLKT","GLNFLKTF"])
        self.assertEqual(cleave_test, ["HGLN", "F", "LKTFKSRF"])
        
        # Test cleavage on subseqs not contained in original.
        cleave_whole = frag.cleave_single("HGLNFLKTFKSRF", ["ABC", "DEF"])
        self.assertEqual(cleave_whole, ["HGLNFLKTFKSRF"])
        
        # Test cleavage on NoneType site list.
        cleave_none = frag.cleave_single("HGLNFLKTFKSRF", [None])
        self.assertEqual(cleave_none, ["HGLNFLKTFKSRF"])
        
        # Test cleavage on empty list of cleavage sites.
        cleave_empty = frag.cleave_single("HGLNFLKTFKSRF", [])
        self.assertEqual(cleave_empty, ["HGLNFLKTFKSRF"])
        
        # Test cleavage with site not in list structure.
        cleave_not_list = frag.cleave_single("HGLNFLKTFKSRF", "KTFK")
        self.assertEqual(cleave_not_list, ["HGLNFLKTFKSRF"])
        
        
    def test_cleave(self):
        # Instantiate Fragmenter object.
        frag = Fragmenter()
        
        # List of test sequences to cleave.
        seq_list = ["AKRHLKGL",
                    "YKLLKLLLPKLKGLLFKL", 
                    "PFWRRRIRIRR", 
                    "GEKLKKIGKKIKNFFQKL", 
                    "RCYCRRRFCVCR", 
                    "KWKSFLKTFKSAKKTVLHTAAKAISS", 
                    "HGLNFLKTFKSRF", 
                    "CAKGVLA"]
        
        # Init cleavage site list.
        site_list = [['AKRHLKGL'], 
                     ['YKLLKLLL', 'LKLLLPKL', 'LKGLLFKL'],
                     ['PFWRRRIR', 'WRRRIRIR'],
                     ['GEKLKKIG', 'KKIGKKIK', 'IKNFFQKL'],
                     ['RCYCRRRF', 'RRRFCVCR'],
                     ['KWKSFLKT', 'FLKTFKSA', 'KKTVLHTA', 'TVLHTAAK', 'TAAKAISS'],
                     ['HGLNFLKT','LNFLKTFK','LKTFKSRF'],
                     [None]]
        
        # Test cleavage on multiple sequences: return list.
        cleave_list = frag.cleave(seq_list, site_list, return_type = "list")
        correct_list = [['AKRH', 'LKGL'],
                        ['YKLL', 'KLL', 'LPKLKGL', 'LFKL'],
                        ['PFWR', 'RR', 'IRIRR'],
                        ['GEKL', 'KKIG', 'KKIKNF', 'FQKL'],
                        ['RCYC', 'RRRF', 'CVCR'],
                        ['KWKS', 'FLKT', 'FKSAKKTV', 'LH', 'TAAK', 'AISS'],
                        ['HGLN', 'FL', 'KTF', 'KSRF'],
                        ['CAKGVLA']]
        self.assertEqual(cleave_list, correct_list)
        self.assertEqual(isinstance(cleave_list, list), True)
        
        # Test cleavage on multiple sequences: return dataframe (success).
        cleave_df = frag.cleave(seq_list, cleave_list, return_type = "dataframe")
        self.assertEqual(isinstance(cleave_df, pandas.DataFrame), True)
        
        # Test cleavage on multiple sequences: return dictionary (success).
        cleave_dict = frag.cleave(seq_list, cleave_list, return_type = "dictionary")
        self.assertEqual(isinstance(cleave_dict, dict), True)
        
        
if (__name__ == "__main__"):
    unittest.main()